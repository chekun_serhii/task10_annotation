package com.chekushka.View.Entity;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

    void print() throws IOException;

}

